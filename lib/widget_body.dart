import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';

class GFMultiSelect<T> extends StatefulWidget {
  const GFMultiSelect({
    required this.items,
    this.dropdownTitleTileTextStyle =
        const TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
    this.color = Colors.transparent,
    this.padding = const EdgeInsets.all(5),
    this.margin = const EdgeInsets.all(5),
    this.size = GFSize.SMALL,
    this.type = GFCheckboxType.basic,
    this.activeBgColor = Colors.black26,
    this.inactiveBgColor = GFColors.WHITE,
    this.activeBorderColor = Colors.black12,
    this.inactiveBorderColor = Colors.red,
    this.listItemTextColor = GFColors.DARK,
    this.buttonColor = GFColors.PRIMARY,
    this.submitButton,
    this.cancelButton,
    this.expandedIcon = const Icon(
      Icons.keyboard_arrow_down,
      color: Colors.black87,
      size: 30,
    ),
    this.collapsedIcon = const Icon(
      Icons.keyboard_arrow_up,
      color: Colors.black87,
      size: 30,
    ),
    this.dropdownBgColor = Colors.black26,
    this.activeIcon = const Icon(
      Icons.check,
      size: 20,
      color: Colors.purple,
    ),
    this.selected = false,
    this.dropdownTitleTileColor = Colors.accents,
    this.hideDropdownUnderline = false,
    this.dropdownTitleTileMargin = const EdgeInsets.all(16),
    this.dropdownTitleTilePadding = const EdgeInsets.all(12),
    this.dropdownButton,
    Key? key,
  }) : super(key: key);

  final List<dynamic> items;
  final TextStyle dropdownTitleTileTextStyle;
  final dynamic dropdownTitleTileColor;
  final bool hideDropdownUnderline;
  final EdgeInsets dropdownTitleTileMargin;
  final EdgeInsets dropdownTitleTilePadding;
  final Widget expandedIcon;
  final Widget collapsedIcon;
  final Widget? submitButton;
  final Widget? cancelButton;
  final Color color;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final GFCheckboxType type;
  final double size;
  final Color activeBgColor;
  final Color inactiveBgColor;
  final Color activeBorderColor;
  final Color inactiveBorderColor;
  final Color buttonColor;
  final Color? listItemTextColor;
  final Widget activeIcon;
  final bool selected;
  final Color dropdownBgColor;
  final Widget? dropdownButton;

  @override
  _GFMultiSelectState createState() => _GFMultiSelectState();
}

class _GFMultiSelectState<T> extends State<GFMultiSelect<T>> {
  bool showDropdown = false;
  final _controller = TextEditingController();
  final List _selectedTitles = [];
  final List _selectedTitlesIndex = [];

  final LayerLink _layerLink = LayerLink();
  late OverlayEntry _overlayEntry;

  @override
  void initState() {
    super.initState();
    if (widget.items.isNotEmpty) {
      _selectedTitles.add(widget.items[1]);
    }
  }

  void _onItemSelect(bool selected, int index) {
    if (selected == true) {
      setState(() {
        _selectedTitles.add(widget.items[index]);
        _selectedTitlesIndex.add(index);
      });
    } else {
      setState(() {
        _selectedTitles.remove(widget.items[index]);
        _selectedTitlesIndex.remove(index);
      });
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  OverlayEntry _showOverlay() {
    final renderBox = context.findRenderObject() as RenderBox;
    var size = renderBox.size;
    final offset = renderBox.localToGlobal(Offset.zero);

    return OverlayEntry(
        builder: (context) => Positioned(
              left: offset.dx,
              top: offset.dy,
              bottom: offset.dy,
              width: size.width,
              child: CompositedTransformFollower(
                link: _layerLink,
                showWhenUnlinked: false,
                offset: Offset(0.0, size.height + 5.0),
                child: Material(
                  elevation: 4.0,
                  child: Container(
                    width: 160,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: widget.dropdownBgColor,
                        border: Border.all(color: Colors.purple)),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Column(
                              children: List.generate(
                                  widget.items.length,
                                  (index) => GFCheckboxListTile(
                                        value: _selectedTitles
                                            .contains(widget.items[index]),
                                        onChanged: (bool selected) {
                                          _controller.text;
                                          _onItemSelect(selected, index);
                                        },
                                        selected: widget.selected,
                                        titleText: widget.items[index],
                                        color: widget.color,
                                        padding: widget.padding,
                                        margin: widget.margin,
                                        size: widget.size,
                                        activeBgColor: widget.activeBgColor,
                                        activeBorderColor:
                                            widget.activeBorderColor,
                                        inactiveBgColor: widget.inactiveBgColor,
                                        activeIcon: widget.activeIcon,
                                        inactiveBorderColor:
                                            widget.inactiveBorderColor,
                                        type: widget.type,
                                      ))),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ));
  }

  Widget build(BuildContext context) {
    Widget dropdownTile() => Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _selectedTitles.length > 1
                ? Expanded(
                    child: Text('Selected EV',
                        style: widget.dropdownTitleTileTextStyle))
                : Expanded(
                    child: Text(_selectedTitles.join().toString(),
                        style: widget.dropdownTitleTileTextStyle)),
            !showDropdown ? widget.expandedIcon : widget.collapsedIcon,
          ],
        );

    return CompositedTransformTarget(
      link: _layerLink,
      child: Center(
        child: Column(
          children: [
            InkWell(
                onTap: () {
                  _overlayEntry = _showOverlay();
                  Overlay.of(context)!.insert(_overlayEntry);
                },
                child: Container(
                    width: 140,
                    decoration: const BoxDecoration(
                      color: Colors.amber,
                    ),
                    child: Column(
                      children: [
                        dropdownTile(),
                        const SizedBox(
                          height: 2,
                        )
                      ],
                    ))),
          ],
        ),
      ),
    );
  }
}














// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:getwidget/getwidget.dart';
// import 'package:joyn/constants/constants.dart' as constants;

// class CheckboxDropdown extends StatefulWidget {
//   const CheckboxDropdown({
//     required this.items,
//     this.size = GFSize.SMALL,
//     this.type = GFCheckboxType.basic,
//     this.activeBgColor = Colors.black26,
//     this.inactiveBgColor = GFColors.WHITE,
//     this.activeBorderColor = Colors.black12,
//     this.inactiveBorderColor = Colors.red,
//     this.listItemTextColor = GFColors.DARK,
//     this.expandedIcon = const Icon(
//       Icons.keyboard_arrow_down,
//       color: Colors.black87,
//       size: 30,
//     ),
//     this.collapsedIcon = const Icon(
//       Icons.keyboard_arrow_up,
//       color: Colors.black87,
//       size: 30,
//     ),
//     this.activeIcon = const Icon(
//       Icons.check,
//       size: 20,
//       color: Colors.purple,
//     ),
//     this.selected = false,
//     this.dropdownTitleTileColor = Colors.accents,
//     this.hideDropdownUnderline = false,
//     this.dropdownTitleTileMargin = const EdgeInsets.all(16),
//     this.dropdownTitleTilePadding = const EdgeInsets.all(12),
//     Key? key,
//   }) : super(key: key);

//   final List<dynamic> items;
//   final dynamic dropdownTitleTileColor;
//   final bool hideDropdownUnderline;
//   final EdgeInsets dropdownTitleTileMargin;
//   final EdgeInsets dropdownTitleTilePadding;
//   final Widget expandedIcon;
//   final Widget collapsedIcon;
//   final GFCheckboxType type;
//   final double size;
//   final Color activeBgColor;
//   final Color inactiveBgColor;
//   final Color activeBorderColor;
//   final Color inactiveBorderColor;
//   final Color? listItemTextColor;
//   final Widget activeIcon;
//   final bool selected;

//   @override
//   _CheckboxDropdown createState() => _CheckboxDropdown();
// }

// class _CheckboxDropdown extends State<CheckboxDropdown> {
//   bool showDropdown = false;
//   final _controller = TextEditingController();
//   final List _selectedTitles = [];
//   final List _selectedTitlesIndex = [];

//   @override
//   void initState() {
//     super.initState();
//     if (widget.items.isNotEmpty) {
//       _selectedTitles.add(widget.items[1]);
//     }
//   }

//   void _onItemSelect(bool selected, int index) {
//     if (selected == true) {
//       setState(() {
//         _selectedTitles.add(widget.items[index]);
//         _selectedTitlesIndex.add(index);
//       });
//     } else {
//       setState(() {
//         _selectedTitles.remove(widget.items[index]);
//         _selectedTitlesIndex.remove(index);
//       });
//     }
//   }

//   @override
//   void dispose() {
//     _controller.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     Widget dropdownTile() => Row(
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: [
//             SvgPicture.asset(constants.Assets.carDropdown),
//             const SizedBox(width: 8),
//             _selectedTitles.length > 1
//                 ? const Expanded(
//                     child: Text('Selected EV',
//                         style: constants.Styles.bigBookTextStyleWhite))
//                 : Expanded(
//                     child: Text(_selectedTitles.join().toString(),
//                         style: constants.Styles.bigBookTextStyleWhite)),
//             SvgPicture.asset(constants.Assets.arrowDropdown),
//           ],
//         );

//     return Center(
//       child: Column(
//         children: [
//           InkWell(
//               onTap: () {
//                 setState(() {
//                   showDropdown = !showDropdown;
//                 });
//               },
//               child: SizedBox(
//                   width: 140,
//                   child: Column(
//                     children: [
//                       dropdownTile(),
//                       const SizedBox(
//                         height: 2,
//                       )
//                     ],
//                   ))),
//           showDropdown
//               ? Container(
//                   width: 160,
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(8),
//                     color: constants.Colors.greyDark,
//                     border: Border.all(color: constants.Colors.purpleMain),
//                   ),
//                   child: SingleChildScrollView(
//                     child: Column(
//                       children: [
//                         Column(
//                           children: List.generate(
//                             widget.items.length,
//                             (index) => Container(
//                               decoration: BoxDecoration(
//                                 border: Border(
//                                   bottom: BorderSide(
//                                     color:
//                                         constants.Colors.white.withOpacity(0.1),
//                                     width: 1,
//                                   ),
//                                 ),
//                               ),
//                               child: GFCheckboxListTile(
//                                 value: _selectedTitles
//                                     .contains(widget.items[index]),
//                                 onChanged: (bool selected) {
//                                   _onItemSelect(selected, index);
//                                 },
//                                 selected: widget.selected,
//                                 title: Text(
//                                   '${widget.items[index]}',
//                                   style: constants.Styles.smallTextStyleWhite,
//                                 ),
//                                 // color: Colors.grey,
//                                 padding:
//                                     const EdgeInsets.only(top: 14, bottom: 13),
//                                 margin:
//                                     const EdgeInsets.only(right: 12, left: 2),
//                                 size: 22,
//                                 activeBgColor: constants.Colors.greyCheckbox,
//                                 activeBorderColor: widget.activeBorderColor,
//                                 inactiveBgColor: constants.Colors.greyCheckbox,
//                                 activeIcon: SvgPicture.asset(
//                                     constants.Assets.checkboxIcon),
//                                 inactiveBorderColor:
//                                     constants.Colors.greyXMiddle,
//                                 type: widget.type,
//                               ),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 )
//               : Container(),
//         ],
//       ),
//     );
//   }
// }



















// import 'package:flutter/material.dart';
// import 'package:getwidget/getwidget.dart';

// class GFMultiSelect<T> extends StatefulWidget {
//   const GFMultiSelect({
//     required this.items,
//     this.dropdownTitleTileTextStyle =
//         const TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
//     this.color = Colors.transparent,
//     this.padding = const EdgeInsets.all(5),
//     this.margin = const EdgeInsets.all(5),
//     this.size = GFSize.SMALL,
//     this.type = GFCheckboxType.basic,
//     this.activeBgColor = Colors.black26,
//     this.inactiveBgColor = GFColors.WHITE,
//     this.activeBorderColor = Colors.black12,
//     this.inactiveBorderColor = Colors.red,
//     this.listItemTextColor = GFColors.DARK,
//     this.buttonColor = GFColors.PRIMARY,
//     this.submitButton,
//     this.cancelButton,
//     this.expandedIcon = const Icon(
//       Icons.keyboard_arrow_down,
//       color: Colors.black87,
//       size: 30,
//     ),
//     this.collapsedIcon = const Icon(
//       Icons.keyboard_arrow_up,
//       color: Colors.black87,
//       size: 30,
//     ),
//     this.dropdownBgColor = Colors.black26,
//     this.activeIcon = const Icon(
//       Icons.check,
//       size: 20,
//       color: Colors.purple,
//     ),
//     this.selected = false,
//     this.dropdownTitleTileColor = Colors.accents,
//     this.hideDropdownUnderline = false,
//     this.dropdownTitleTileMargin = const EdgeInsets.all(16),
//     this.dropdownTitleTilePadding = const EdgeInsets.all(12),
//     this.dropdownButton,
//     Key? key,
//   }) : super(key: key);

//   final List<dynamic> items;
//   final TextStyle dropdownTitleTileTextStyle;
//   final dynamic dropdownTitleTileColor;
//   final bool hideDropdownUnderline;
//   final EdgeInsets dropdownTitleTileMargin;
//   final EdgeInsets dropdownTitleTilePadding;
//   final Widget expandedIcon;
//   final Widget collapsedIcon;
//   final Widget? submitButton;
//   final Widget? cancelButton;
//   final Color color;
//   final EdgeInsets margin;
//   final EdgeInsets padding;
//   final GFCheckboxType type;
//   final double size;
//   final Color activeBgColor;
//   final Color inactiveBgColor;
//   final Color activeBorderColor;
//   final Color inactiveBorderColor;
//   final Color buttonColor;
//   final Color? listItemTextColor;
//   final Widget activeIcon;
//   final bool selected;
//   final Color dropdownBgColor;
//   final Widget? dropdownButton;

//   @override
//   _GFMultiSelectState createState() => _GFMultiSelectState();
// }

// class _GFMultiSelectState<T> extends State<GFMultiSelect<T>> {
//   bool showDropdown = false;
//   final _controller = TextEditingController();
//   final List _selectedTitles = [];
//   final List _selectedTitlesIndex = [];

//   @override
//   void initState() {
//     super.initState();
//     if (widget.items.isNotEmpty) {
//       _selectedTitles.add(widget.items[1]);
//     }
//   }

//   void _onItemSelect(bool selected, int index) {
//     if (selected == true) {
//       setState(() {
//         _selectedTitles.add(widget.items[index]);
//         _selectedTitlesIndex.add(index);
//       });
//     } else {
//       setState(() {
//         _selectedTitles.remove(widget.items[index]);
//         _selectedTitlesIndex.remove(index);
//       });
//     }
//   }

//   @override
//   void dispose() {
//     _controller.dispose();
//     super.dispose();
//   }

//   _showOverlay(BuildContext context) {
//     OverlayState? overlayState = Overlay.of(context);
//     OverlayEntry? overlayEntry;
//     overlayEntry = OverlayEntry(builder: (context) {
//       return Positioned(
//         left: MediaQuery.of(context).size.width * 0.2,
//         top: MediaQuery.of(context).size.height * 0.4,
//         child: Container(
//           width: MediaQuery.of(context).size.width * 0.8,
//           child: Stack(
//             children: [
//               Positioned(
//                 child: Material(
//                   child: Container(
//                     width: 160,
//                     decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(8),
//                         color: widget.dropdownBgColor,
//                         border: Border.all(color: Colors.purple)),
//                     child: SingleChildScrollView(
//                       child: Column(
//                         children: [
//                           Column(
//                               children: List.generate(
//                                   widget.items.length,
//                                   (index) => GFCheckboxListTile(
//                                         value: _selectedTitles
//                                             .contains(widget.items[index]),
//                                         onChanged: (bool selected) {
//                                           _controller.text;
//                                           _onItemSelect(selected, index);
//                                         },
//                                         selected: widget.selected,
//                                         titleText: widget.items[index],
//                                         color: widget.color,
//                                         padding: widget.padding,
//                                         margin: widget.margin,
//                                         size: widget.size,
//                                         activeBgColor: widget.activeBgColor,
//                                         activeBorderColor:
//                                             widget.activeBorderColor,
//                                         inactiveBgColor: widget.inactiveBgColor,
//                                         activeIcon: widget.activeIcon,
//                                         inactiveBorderColor:
//                                             widget.inactiveBorderColor,
//                                         type: widget.type,
//                                       ))),
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       );
//     });

//     overlayState!.insert(overlayEntry);
//   }

//   @override
//   Widget build(BuildContext context) {
//     Widget dropdownTile() => Row(
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: [
//             _selectedTitles.length > 1
//                 ? Expanded(
//                     child: Text('Selected EV',
//                         style: widget.dropdownTitleTileTextStyle))
//                 : Expanded(
//                     child: Text(_selectedTitles.join().toString(),
//                         style: widget.dropdownTitleTileTextStyle)),
//             !showDropdown ? widget.expandedIcon : widget.collapsedIcon,
//           ],
//         );

//     return Center(
//       child: Column(
//         children: [
//           InkWell(
//               onTap: () {
//                 setState(() {
//                   _showOverlay(context);
//                 });
//               },
//               child: Container(
//                   width: 140,
//                   decoration: const BoxDecoration(
//                     color: Colors.amber,
//                   ),
//                   child: Column(
//                     children: [
//                       dropdownTile(),
//                       const SizedBox(
//                         height: 2,
//                       )
//                     ],
//                   ))),
//         ],
//       ),
//     );
//   }
// }
