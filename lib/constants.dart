import 'dart:ui';

mixin Colors {
  static const white = Color(0xffFFFFFF);
  static const whiteDark = Color(0xffE3E3E3);
  static const greyDark = Color(0xff383747);
  static const greyXDark = Color(0xff3A3747);
  static const greyMiddle = Color(0xff4B4859);
  static const greyXMiddle = Color(0xff696B7E);
  static const greyLight = Color(0xff7C7F8A);
  static const greyXLight = Color(0xffC7C6D1);
  static const purpleMain = Color(0xff8F7BFF);
  static const purpleMainDark = Color(0xff584D96);
  static const purpleDark = Color(0xff534C75);
  static const purpleXDark = Color(0xff1F1D2A);
  static const purpleLight = Color(0xff4D476C);
  static const purpleXLight = Color(0xff78728F);
  static const purpleXxLight = Color(0xff847DA5);
  static const blueXDark = Color(0xff27233E);
  static const blueXxDark = Color(0xff1E1A32);
  static const blueXxxDark = Color(0xFF181524);
  static const gold = Color(0xffFFBA02);
  static const red = Color(0xffFB4748);
  static const green = Color(0xff47FBEA);
  static const greenPale = Color(0xff48BAB5);
  static const black = Color(0xff000000);
  static const greenLight = Color(0xffB3FDF6);
  static const greenXLight = Color(0xffCFFFFA);
  static const greenDark = Color(0xff2D495A);
  static const blueXxxxDark = Color(0xff191527);
}
