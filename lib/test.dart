import 'package:flutter/material.dart';

class TabBarWidget extends StatelessWidget {
  const TabBarWidget(
      {Key? key,
      required this.tabs,
      this.padding = const EdgeInsets.symmetric(vertical: 10, horizontal: 18),
      this.labelStyle,
      this.unselectedLabelStyle,
      this.indicatorColor})
      : super(key: key);

  final List<Tab> tabs;
  final EdgeInsets padding;
  final TextStyle? labelStyle;
  final TextStyle? unselectedLabelStyle;
  final Color? indicatorColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          const SizedBox(
            height: 800,
            width: 333,
          ),
          Padding(
            padding: const EdgeInsets.all(100.0),
            child: Container(
              width: 111,
              decoration: const BoxDecoration(
                color: Colors.yellow,
              ),
            ),
          ),
          Column(
            children: [
              Container(
                width: 111,
                decoration: const BoxDecoration(
                  color: Colors.amber,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
