import 'package:flutter/material.dart';
import 'package:test_widgets/widget_body.dart';

class TestWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: const Text('Bla bla car'),
        ),
        body: const GFMultiSelect(
          items: [
            "All EV's",
            'Main EV',
            '<EV2>',
          ],
        ),
        // body: TestPage(),
      );
}
