// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:getwidget/getwidget.dart';
// import 'package:dropdown_button2/dropdown_button2.dart';
// import 'package:joyn/constants/constants.dart' as constants;

// class CheckboxDropdown extends StatefulWidget {
//   List<String> items;
//   SvgPicture? icon;
//   double width;

//   CheckboxDropdown({
//     Key? key,
//     required this.items,
//     this.icon,
//     required this.width,
//   }) : super(key: key);

//   @override
//   State<CheckboxDropdown> createState() => _CheckboxDropdown();
// }

// class _CheckboxDropdown extends State<CheckboxDropdown> {
//   String? selectedValue;
//   bool selected = false;

//   final List _selectedTitles = [];
//   final List _selectedTitlesIndex = [];

//   final GFCheckboxType type = GFCheckboxType.basic;

//   @override
//   void initState() {
//     super.initState();
//     if (widget.items.isNotEmpty) {
//       _selectedTitles.add(widget.items[1]);
//     }
//   }

//   void _onItemSelect(bool selected, int index) {
//     if (selected == true) {
//       setState(() {
//         _selectedTitles.add(widget.items[index]);
//         _selectedTitlesIndex.add(index);
//       });
//     } else {
//       setState(() {
//         _selectedTitles.remove(widget.items[index]);
//         _selectedTitlesIndex.remove(index);
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: widget.width,
//       child: DropdownButtonHideUnderline(
//         child: DropdownButton2(
//           items: List.generate(
//             widget.items.length,
//             (index) => DropdownMenuItem<String>(
//               value: widget.items[index],
//               child: Container(
//                 decoration: BoxDecoration(
//                   border: Border(
//                     bottom: BorderSide(
//                       color: Colors.white.withOpacity(0.1),
//                       width: 1,
//                     ),
//                   ),
//                 ),
//                 child: StatefulBuilder(
//                   builder: (context, setStateSB) => GFCheckboxListTile(
//                     value: _selectedTitles.contains(widget.items[index]),
//                     onChanged: (bool selected) {
//                       _onItemSelect(selected, index);
//                       setStateSB(() {});
//                     },
//                     selected: selected,
//                     title: Text(
//                       widget.items[index],
//                       style: constants.Styles.smallTextStyleWhite,
//                     ),
//                     padding: const EdgeInsets.only(top: 12, bottom: 13),
//                     margin: const EdgeInsets.only(right: 0, left: 0),
//                     size: 22,
//                     activeBgColor: constants.Colors.greyCheckbox,
//                     activeBorderColor: constants.Colors.greyXMiddle,
//                     inactiveBgColor: constants.Colors.greyCheckbox,
//                     activeIcon: SvgPicture.asset(constants.Assets.checkboxIcon),
//                     inactiveBorderColor: constants.Colors.greyXMiddle,
//                     type: type,
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           hint: Row(
//             children: [
//               SvgPicture.asset(constants.Assets.carDropdown),
//               const SizedBox(width: 8),
//               _selectedTitles.length > 1
//                   ? const Text('Selecte EV',
//                       style: constants.Styles.xSmallLtStdTextStyleWhite)
//                   : Text(_selectedTitles.join().toString(),
//                       style: constants.Styles.bigBookTextStyleWhite),
//             ],
//           ),
//           value: selectedValue,
//           onChanged: (value) {
//             setState(() {
//               selectedValue = value as String;
//             });
//           },
//           icon: SvgPicture.asset(constants.Assets.arrowDropdown),
//           iconSize: 21,
//           buttonHeight: 27,
//           itemHeight: 47,
//           dropdownMaxHeight: 185,
//           dropdownWidth: 140,
//           dropdownDecoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(8),
//               border: Border.all(
//                 color: constants.Colors.purpleMain,
//               ),
//               color: constants.Colors.greyDark),
//           selectedItemBuilder: (context) {
//             return widget.items.map(
//               (item) {
//                 return Row(
//                   children: [
//                     widget.icon ?? const SizedBox(),
//                     const SizedBox(width: 8),
//                     Text(
//                       item,
//                       style: constants.Styles.bigBookTextStyleWhite,
//                     ),
//                   ],
//                 );
//               },
//             ).toList();
//           },
//         ),
//       ),
//     );
//   }
// }









//PopupMenu---------------------------------------------------------------------

// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:getwidget/getwidget.dart';
// import 'package:joyn/constants/constants.dart' as constants;

// class CheckboxDropdown extends StatefulWidget {
//   List<String> items;
//   SvgPicture? icon;
//   double width;

//   CheckboxDropdown({
//     Key? key,
//     required this.items,
//     required this.icon,
//     required this.width,
//   }) : super(key: key);

//   @override
//   State<CheckboxDropdown> createState() => _CheckboxDropdown();
// }

// class _CheckboxDropdown extends State<CheckboxDropdown> {
//   String? selectedValue;
//   bool selected = false;

//   final List _selectedTitles = [];
//   final List _selectedTitlesIndex = [];

//   @override
//   void initState() {
//     super.initState();
//     if (widget.items.isNotEmpty) {
//       _selectedTitles.add(widget.items[1]);
//     }
//   }

//   void _onItemSelect(bool selected, int index) {
//     if (selected == true) {
//       setState(() {
//         _selectedTitles.add(widget.items[index]);
//         _selectedTitlesIndex.add(index);
//       });
//     } else {
//       setState(() {
//         _selectedTitles.remove(widget.items[index]);
//         _selectedTitlesIndex.remove(index);
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: widget.width,
//       child: PopupMenuButton(
//           offset: const Offset(0, 40),
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: [
//                 widget.icon ?? const SizedBox(),
//                 const SizedBox(width: 8),
//                 Flexible(
//                   child: _selectedTitles.length > 1
//                       ? const Text('Selected EV',
//                           style: constants.Styles.xSmallLtStdTextStyleWhite)
//                       : Text(
//                           _selectedTitles.join().toString(),
//                           style: constants.Styles.bigBookTextStyleWhite,
//                         ),
//                 ),
//                 const SizedBox(width: 5),
//                 SvgPicture.asset(constants.Assets.arrowDropdown),
//               ],
//             ),
//           ),
//           itemBuilder: (context) => List.generate(
//                 widget.items.length,
//                 (index) => PopupMenuItem(
//                   padding: EdgeInsets.zero,
//                   value: widget.items[index],
//                   child: Container(
//                     decoration: BoxDecoration(
//                       color: constants.Colors.greyDark,
//                       border: Border(
//                         bottom: BorderSide(
//                           color: constants.Colors.white.withOpacity(0.1),
//                           width: 1,
//                         ),
//                       ),
//                     ),
//                     child: StatefulBuilder(
//                       builder: (context, setStateSB) => GFCheckboxListTile(
//                         value: _selectedTitles.contains(widget.items[index]),
//                         onChanged: (bool? selected) {
//                           _onItemSelect(selected!, index);
//                           setStateSB(() {});
//                         },
//                         selected: selected,
//                         title: Text(
//                           widget.items[index],
//                         ),
//                         padding: const EdgeInsets.only(top: 12, bottom: 13),
//                         margin: const EdgeInsets.only(right: 0, left: 0),
//                         size: 22,
//                         activeBgColor: constants.Colors.greyCheckbox,
//                         activeBorderColor: constants.Colors.greyCheckbox,
//                         inactiveBgColor: constants.Colors.greyCheckbox,
//                         activeIcon:
//                             SvgPicture.asset(constants.Assets.checkboxIcon),
//                         inactiveBorderColor: constants.Colors.greyXMiddle,
//                         type: GFCheckboxType.basic,
//                       ),
//                     ),
//                   ),
//                 ),
//               )),
//     );
//   }
// }







//PopupMenu with button---------------------------------------------------------


// String? selectedValue;

//   final List<String> _selectedTitles = [];
//   final List<int> _selectedTitlesIndex = [];

//   // final GFCheckboxType type = GFCheckboxType.basic;

//   @override
//   void initState() {
//     super.initState();
//     // if (widget.items.isNotEmpty) {
//     //   _selectedTitles.add(widget.items[0]);
//     // }
//   }

//   void _onItemSelect(bool selected, int index) {
//     setState(() {
//       if (selected == true) {
//         _selectedTitles.add(widget.items[index]);
//         _selectedTitlesIndex.add(index);
//       } else {
//         _selectedTitles.remove(widget.items[index]);
//         _selectedTitlesIndex.remove(index);
//       }
//     });
//   }

//   void _selectAll(bool selected) {
//     setState(() {
//       if (selected == true) {
//         _selectedTitles.clear();
//         _selectedTitles.addAll(widget.items);
//         _selectedTitlesIndex.clear();
//         for (int i = 0; i <= widget.items.length; i++) {
//           _selectedTitlesIndex.add(i);
//         }
//       } else {
//         _selectedTitles.clear();
//         _selectedTitlesIndex.clear();
//       }
//       Navigator.of(context).pop();
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: 150,
//       child: PopupMenuButton(
//           offset: Offset(0, 50),
//           child: Padding(
//             padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.end,
//               children: [
//                 if (_selectedTitles.isNotEmpty) Text("<"),
//                 Flexible(
//                   child: Text(
//                     _selectedTitles.isEmpty
//                         ? "Select a value"
//                         : _selectedTitles
//                             .map((item) => item)
//                             .toList()
//                             .join(", "),
//                     overflow: TextOverflow.ellipsis,
//                   ),
//                 ),
//                 if (_selectedTitles.isNotEmpty) Text(">"),
//                 SizedBox(width: 5),
//                 Icon(Icons.arrow_drop_down)
//               ],
//             ),
//           ),
//           itemBuilder: (context) {
//             List<PopupMenuEntry> items1 = List.generate(
//               widget.items.length,
//               (index) => PopupMenuItem(
//                 value: widget.items[index],
//                 child: Container(
//                   decoration: BoxDecoration(
//                     border: Border(
//                       bottom: BorderSide(
//                         // color: constants.Colors.white.withOpacity(0.1),
//                         width: 1,
//                       ),
//                     ),
//                   ),
//                   child: StatefulBuilder(
//                     builder: (context, setStateSB) => CheckboxListTile(
//                       value: _selectedTitles.contains(widget.items[index]),
//                       onChanged: (bool? selected) {
//                         _onItemSelect(selected!, index);
//                         setStateSB(() {});
//                       },
//                       selected: false,
//                       title: Text(
//                         widget.items[index],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             );
//             return [
//               PopupMenuItem(
//                 value: "select all",
//                 child: Container(
//                   decoration: BoxDecoration(
//                     border: Border(
//                       bottom: BorderSide(
//                         // color: constants.Colors.white.withOpacity(0.1),
//                         width: 1,
//                       ),
//                     ),
//                   ),
//                   child: Row(
//                     children: [
//                       TextButton(
//                         child: Text(
//                           "Select all",
//                         ),
//                         onPressed: () {
//                           _selectAll(true);
//                         },
//                       ),
//                       Spacer(),
//                       TextButton(
//                         child: Text(
//                           "Unselect all",
//                         ),
//                         onPressed: () {
//                           _selectAll(false);
//                         },
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//               ...items1
//             ];
//           }),
//     );
//   }
// }
